import request from '@/utils/request'

const api = {
  tenant: '/system/tenant'
}

// 查询租户管理列表
export function listTenant(query) {
  return request({
    url: api.tenant+'/list',
    method: 'get',
    params: query
  })
}


// 删除租户管理
export function delTenant(ids) {
  return request({
    url: api.tenant + "/delete/" + ids,
    method: 'post'
  })
}

// 新增或修改客户信息
export function saveTenant(data) {
  return request({
    url: api.tenant + (data.id > 0 ? '/update' : '/save'),
    method: 'post',
    data: data
  })
}
